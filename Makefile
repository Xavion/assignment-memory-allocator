CFLAGS = --std=gnu17 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG
BUILDDIR=build
SRCDIR=src
CC=gcc

MKDIR=mkdir -p
RM=rm -rf


all: $(BUILDDIR)/mem.o $(BUILDDIR)/util.o $(BUILDDIR)/mem_debug.o $(BUILDDIR)/test_malloc.o $(BUILDDIR)/main.o
	$(CC) -o $(BUILDDIR)/main $^

exec: all
	./$(BUILDDIR)/main

$(BUILDDIR):
	$(MKDIR) $(BUILDDIR)

$(BUILDDIR)/%.o: $(SRCDIR)/%.c build
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	$(RM) $(BUILDDIR)
