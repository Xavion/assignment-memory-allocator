#include "test_malloc.h"
#include "util.h"

static void test_1() {
    void *malloc_1 = _malloc(100);
    void *malloc_2 = _malloc(200);
    void *malloc_3 = _malloc(500);
    struct block_header *block_1 = block_get_header(malloc_1);
    struct block_header *block_2 = block_get_header(malloc_2);
    struct block_header *block_3 = block_get_header(malloc_3);
    debug_heap(stdout, block_1);
    bool check_1 = !block_1->is_free;
    bool check_2 = !block_2->is_free;
    bool check_3 = !block_3->is_free;
    if (check_1 && check_2 && check_3) {
        _free(malloc_1);
        _free(malloc_2);
        _free(malloc_3);
        debug_heap(stdout, block_1);
    }
}


static void test_2(){
     void *malloc_1 = _malloc(100);
     void *malloc_2 = _malloc(200);
     void *malloc_3 = _malloc(500);
     struct block_header *block_1 = block_get_header(malloc_1);
     struct block_header *block_2 = block_get_header(malloc_2);
     struct block_header *block_3 = block_get_header(malloc_3);
     debug_heap(stdout, block_1);
     _free(malloc_1);
     debug_heap(stdout, block_1);
     bool check_1 = block_1->is_free;
     bool check_2 = !block_2->is_free;
     bool check_3 = !block_3->is_free;
     if (check_1 && check_2 && check_3) {
         _free(malloc_2);
         _free(malloc_3);
         debug_heap(stdout, block_1);
     }
}
static void test_3(){
    void *malloc_1 = _malloc(100);
    void *malloc_2 = _malloc(200);
    void *malloc_3 = _malloc(500);
    struct block_header *block_1 = block_get_header(malloc_1);
    struct block_header *block_2 = block_get_header(malloc_2);
    struct block_header *block_3 = block_get_header(malloc_3);
    debug_heap(stdout, block_1);
    _free(malloc_1);
    debug_heap(stdout, block_1);
    _free(malloc_2);
    debug_heap(stdout, block_1);
    bool check_1 = block_1->is_free;
    bool check_2 = block_2->is_free;
    bool check_3 = !block_3->is_free;
    if (check_1 && check_2 && check_3) {
        _free(malloc_3);
        debug_heap(stdout, block_1);
    } 
}
static void test_4(){
    void *malloc_1 = _malloc(7000);
    void *malloc_2 = _malloc(4000);
    struct block_header *block_1 = block_get_header(malloc_1);
    struct block_header *block_2 = block_get_header(malloc_2);
    void*  tmp = block_1->contents + block_1->capacity.bytes;
    debug_heap(stdout, block_1);
    bool check_1 = !block_1->is_free;
    bool check_2 = !block_2->is_free;
    bool check_3 = block_2 == tmp ;
    if (check_1 && check_2 && check_3) {
        _free(malloc_1);
        _free(malloc_2);
        debug_heap(stdout, block_1);
    }
}
static void test_5(){
    void *malloc_1 = _malloc(12000);
    struct block_header *block_1 = block_get_header(malloc_1);
    debug_heap(stdout, block_1);
    struct block_header* last_block = block_1;
    while(last_block->next){
        last_block = last_block->next;
    }
    void*  tmp = last_block->contents + last_block->capacity.bytes;
    mmap(tmp, 256, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
    void *malloc_2 = _malloc(7000);
    debug_heap(stdout, block_1);
    struct block_header *block_2 = block_get_header(malloc_2);
    bool check_1 = !block_1->is_free;
    bool check_2 = !block_2->is_free;
    bool check_3 = tmp != block_2;
    if (check_1 && check_2 && check_3) {
        _free(malloc_1);
        _free(malloc_2);
        debug_heap(stdout, block_1);
    }
}

void test_prog(){
    heap_init(800);
    test_1();
    test_2();
    test_3();
    test_4();
    test_5();

}
